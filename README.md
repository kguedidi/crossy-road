# Projet Crossy Road

## Auteurs
- **Fouad Belhia**
- **Zinedine OUDINI**

## Description du Projet
Ce projet consiste à développer une version personnalisée du jeu "Crossy Road" en utilisant le langage de programmation C# et Unity. Le but du jeu est d'avancer autant que possible à travers des routes, des rivières, de l'herbe et des voies ferrées, sans mourir. Le joueur contrôle une mascotte et doit éviter divers obstacles tels que les voitures, les trains et les rivières.

## Fonctionnalités du Jeu
- **Génération automatique de la carte** : 
  - Terrain fixe (herbe avec buissons ou arbres)
  - Terrain avec objet mouvant (route avec voitures ou camions)
  - Terrain non franchissable (rivière avec troncs d'arbres et nénuphars)
- **Interface Utilisateur** :
  - Menu de démarrage (Jouer, Choix de difficulté, Activer/Désactiver le son, Quitter)
  - Menu de pause (Continuer, Recommencer, Quitter)
  - Interface de jeu (score, temps écoulé, meilleur score)