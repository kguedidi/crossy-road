using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movable : MonoBehaviour
{
    [SerializeField] protected float speed;
    protected int extent;

    protected virtual void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if (IsOutOfBounds())
        {
            Destroy(gameObject);
        }
    }

    public virtual void Setup(int extent)
    {
        this.extent = extent;
    }

    protected abstract bool IsOutOfBounds();

    public float GetSpeed()
    {
        return speed;
    }
}
