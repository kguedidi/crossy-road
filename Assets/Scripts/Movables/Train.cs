using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NewBehaviourScript;

public class Train : Movable
{
    private void Start()
    {
        SetSpeedBasedOnDifficulty();
    }

    private void SetSpeedBasedOnDifficulty()
    {
        DifficultyLevel difficulty = GameManager.Instance.GetCurrentDifficulty();
        switch (difficulty)
        {
            case DifficultyLevel.Easy:
                speed = 5.0f; 
                break;
            case DifficultyLevel.Medium:
                speed = 10.0f; 
                break;
            case DifficultyLevel.Hard:
                speed = 15.0f; 
                break;
        }
    }

    protected override bool IsOutOfBounds()
    {
        return transform.position.x < -(extent + 5) || transform.position.x > extent + 5;
    }
}
