using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NewBehaviourScript;

public class Trunk : Movable
{
    public bool isRight;

    private void Start()
    {
        SetSpeedBasedOnDifficulty();
    }

    private void SetSpeedBasedOnDifficulty()
    {
        DifficultyLevel difficulty = GameManager.Instance.GetCurrentDifficulty();
        switch (difficulty)
        {
            case DifficultyLevel.Easy:
                speed = Random.Range(1, 2);
                break;
            case DifficultyLevel.Medium:
                speed = Random.Range(3, 4);
                break;
            case DifficultyLevel.Hard:
                speed = Random.Range(6, 7);
                break;
        }
    }

    protected override bool IsOutOfBounds()
    {
        return transform.position.x < -(extent + 1) || transform.position.x > extent + 1;
    }
}
