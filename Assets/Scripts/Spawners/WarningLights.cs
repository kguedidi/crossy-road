using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningLights : MonoBehaviour
{
    [SerializeField] MeshRenderer warningLight1;
    [SerializeField] MeshRenderer warningLight2;
    [SerializeField] Material lightOn;
    [SerializeField] Material lightOff;
    public float CurrentTimer { get; set; }

    public void Update()
    {
            if ((CurrentTimer <= 2f && CurrentTimer > 1.75f) ||
                (CurrentTimer <= 1.5f && CurrentTimer > 1.25f) ||
                (CurrentTimer <= 1f && CurrentTimer > 0.75f) ||
                (CurrentTimer <= 0.5f && CurrentTimer > 0.25f) ||
                CurrentTimer <= 0)
            {
                warningLight1.material = lightOn;
                warningLight2.material = lightOff;
            }
            else if ((CurrentTimer <= 1.75f && CurrentTimer > 1.5f) ||
                     (CurrentTimer <= 1.25f && CurrentTimer > 1f) ||
                     (CurrentTimer <= 0.75f && CurrentTimer > 0.5f) ||
                     (CurrentTimer <= 0.25f && CurrentTimer > 0f))
            {
                warningLight1.material = lightOff;
                warningLight2.material = lightOn;
            }
        }

    public void TurnOn()
    {
        warningLight1.material = lightOn;
        warningLight2.material = lightOn;
    }

    public void TurnOff()
    {
        warningLight1.material = lightOff;
        warningLight2.material = lightOff;
    }
}