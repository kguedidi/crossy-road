using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrunkSpawner : MovableSpawner
{
    [SerializeField] GameObject trunkPrefab_S, trunkPrefab_M, trunkPrefab_L;
    public List<int> safeXPos = new List<int>();
    TrunkType whichTrunk;
    public bool IsRight
    {
        get { return isRight; }
        set { isRight = value; }
    }

    protected override void Start()
    {
        base.Start();
        RandomTrunk();
    }

    protected override void Spawn()
    {
        var spawnPos = this.transform.position +
            Vector3.right * (isRight ? -(terrain.Extent + 1) : terrain.Extent + 1);

        GameObject chosenTrunk = GetTrunkPrefab();

        var trunkInstance = Instantiate
        (
            original: chosenTrunk,
            position: spawnPos,
            rotation: Quaternion.Euler(0, isRight ? 90 : -90, 0),
            parent: this.transform
        );

        var trunk = trunkInstance.GetComponent<Trunk>();
        trunk.Setup(terrain.Extent);
        trunk.isRight = isRight;

        RandomTrunk();
    }

    private GameObject GetTrunkPrefab()
    {
        switch (whichTrunk)
        {
            case TrunkType.Small:
                return trunkPrefab_S;
            case TrunkType.Medium:
                return trunkPrefab_M;
            case TrunkType.Large:
                return trunkPrefab_L;
            default:
                return trunkPrefab_S;
        }
    }

    private void RandomTrunk()
    {
        float randomTrunk = Random.value;

        if (randomTrunk <= 0.33f)
        {
            whichTrunk = TrunkType.Small;
        }
        else if (randomTrunk <= 0.66f)
        {
            whichTrunk = TrunkType.Medium;
        }
        else
        {
            whichTrunk = TrunkType.Large;
        }
    }
}

public enum TrunkType
{
    Small,
    Medium,
    Large
}
