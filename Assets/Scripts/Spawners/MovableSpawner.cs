using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MovableSpawner : MonoBehaviour
{
    [SerializeField] protected TerrainBlock terrain;
    [SerializeField] protected float minSpawnDuration = 2;
    [SerializeField] protected float maxSpawnDuration = 4;
    protected bool isRight;
    protected float timer;

    protected virtual void Start()
    {
        isRight = Random.value > 0.5f;
        timer = Random.Range(minSpawnDuration, maxSpawnDuration);
    }

    protected virtual void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            return;
        }

        timer = Random.Range(minSpawnDuration, maxSpawnDuration);
        Spawn();
    }

    protected abstract void Spawn();
}