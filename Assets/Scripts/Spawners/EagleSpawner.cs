using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static NewBehaviourScript;

public class EagleSpawner : MonoBehaviour
{
    [SerializeField] GameObject eaglePrefab;
    [SerializeField] int spawnZPos = 7;
    [SerializeField] Player player;
    [SerializeField] float easyTimer = 10f;
    [SerializeField] float mediumTimer = 7f;
    [SerializeField] float hardTimer = 5f; 
    private float timer;
    private float timerMax;

    int playerLastMaxTravel = 0;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.Instance;
        SetTimerBasedOnDifficulty();
    }

    void Update()
    {
        if (player.MaxTravel != playerLastMaxTravel)
        {
            timer = timerMax;
            playerLastMaxTravel = player.MaxTravel;
            return;
        }

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            return;
        }

        if (player.IsBouncing() == false && player.IsDie == false)
        {
            SpawnEagle();
        }
    }

    public void SetTimerBasedOnDifficulty()
    {
        DifficultyLevel difficulty = gameManager.GetCurrentDifficulty();
        switch (difficulty)
        {
            case DifficultyLevel.Easy:
                timerMax = easyTimer;
                break;
            case DifficultyLevel.Medium:
                timerMax = mediumTimer;
                break;
            case DifficultyLevel.Hard:
                timerMax = hardTimer;
                break;
        }
        timer = timerMax;
        Debug.Log("Difficulty: " + difficulty + ", TimerMax: " + timerMax); 
    }

    private void SpawnEagle()
    {
        SoundManager.Instance.Eagle();
        player.enabled = false;
        var position = new Vector3(player.transform.position.x, 1, player.CurrentTravel + spawnZPos);
        var rotation = Quaternion.Euler(0, 180, 0);
        var eagleObject = Instantiate(eaglePrefab, position, rotation);
        var eagle = eagleObject.GetComponent<Eagle>();
        eagle.SetUpTarget(player);
    }
}
