using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockSpawner : MonoBehaviour
{
    [SerializeField] GameObject rockPrefab;
    [SerializeField] TerrainBlock terrain;
    [SerializeField] int count = 1;
    [SerializeField] float spawnProbability = 0.33f;

    void Start()
    {
        if (ShouldSpawnRocks())
            SpawnRocks();
    }

    bool ShouldSpawnRocks()
    {
        return Random.value <= spawnProbability;
    }

    void SpawnRocks()
    {
        List<Vector3> emptyPositions = GetEmptyPositions();

        for (int i = 0; i < count; i++)
        {
            if (emptyPositions.Count > 0)
            {
                int index = Random.Range(0, emptyPositions.Count);
                Vector3 spawnPos = emptyPositions[index];
                SpawnRock(spawnPos);
                emptyPositions.RemoveAt(index);
            }
        }
    }

    List<Vector3> GetEmptyPositions()
    {
        List<Vector3> emptyPositions = new List<Vector3>();

        for (int x = -terrain.Extent; x <= terrain.Extent; x++)
        {
            if (transform.position.z == 0 && x == 0)
                continue;

            emptyPositions.Add(transform.position + Vector3.right * x);
        }

        return emptyPositions;
    }

    void SpawnRock(Vector3 position)
    {
        Instantiate(rockPrefab, position, Quaternion.identity, transform);
    }
}
