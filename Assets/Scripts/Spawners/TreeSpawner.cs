using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpawner : MonoBehaviour
{
    [SerializeField] GameObject treePrefab;
    [SerializeField] TerrainBlock terrain;
    [SerializeField] int count = 3;

    void Start()
    {
        SpawnTrees();
    }

    void SpawnTrees()
    {
        List<Vector3> emptyPositions = GetEmptyPositions();

        for (int i = 0; i < count; i++)
        {
            if (emptyPositions.Count > 0)
            {
                int index = Random.Range(0, emptyPositions.Count);
                Vector3 spawnPos = emptyPositions[index];
                SpawnTree(spawnPos);
                emptyPositions.RemoveAt(index);
            }
        }

        SpawnTreeAtExtremities();
    }

    List<Vector3> GetEmptyPositions()
    {
        List<Vector3> emptyPositions = new List<Vector3>();

        for (int x = -terrain.Extent; x <= terrain.Extent; x++)
        {
            if (transform.position.z == 0 && x == 0)
                continue;

            emptyPositions.Add(transform.position + Vector3.right * x);
        }

        return emptyPositions;
    }

    void SpawnTree(Vector3 position)
    {
        Instantiate(treePrefab, position, Quaternion.identity, transform);
    }

    void SpawnTreeAtExtremities()
    {
        SpawnTree(transform.position + Vector3.right * -(terrain.Extent + 1));
        SpawnTree(transform.position + Vector3.right * (terrain.Extent + 1));
    }
}
