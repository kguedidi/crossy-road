using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MovableSpawner
{
    [SerializeField] GameObject carPrefab;

    protected override void Spawn()
    {
        var spawnPos = this.transform.position +
            Vector3.right * (isRight ? -(terrain.Extent + 1) : terrain.Extent + 1);

        var carInstance = Instantiate
        (
            original: carPrefab,
            position: spawnPos,
            rotation: Quaternion.Euler(0, isRight ? 90 : -90, 0),
            parent: this.transform
        );

        var car = carInstance.GetComponent<Car>();
        car.Setup(terrain.Extent);
    }
}
