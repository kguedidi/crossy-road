using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LilyPadSpawner : MonoBehaviour
{
    [SerializeField] GameObject lilypadPrefab;
    [SerializeField] TerrainBlock terrain;
    [SerializeField] Water water;
    [SerializeField] int count = 5; 
    public List<int> safeXPos = new List<int>();

    void Start()
    {
        List<Vector3> emptyPos = new List<Vector3>();

        for (int x = -terrain.Extent; x <= terrain.Extent; x++)
        {
            if (transform.position.z == 0 && x == 0)
            {
                continue;
            }

            emptyPos.Add(transform.position + Vector3.right * x);
        }

        for (int i = 0; i < count; i++)
        {
            if (emptyPos.Count == 0)
            {
                break;
            }

            var index = Random.Range(0, emptyPos.Count);
            var spawnPos = emptyPos[index];

            Instantiate(lilypadPrefab, spawnPos, Quaternion.identity, this.transform);
            safeXPos.Add((int)spawnPos.x);

            emptyPos.RemoveAt(index);

            Debug.Log($"Lily pad spawned at position {spawnPos}");
        }

        if (safeXPos.Count == 0)
        {
            Debug.LogWarning("No lily pads were spawned.");
        }
    }
}
