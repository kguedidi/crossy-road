using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainSpawner : MovableSpawner
{
    [SerializeField] GameObject trainPrefab;
    [SerializeField] WarningLights warningLights;
    bool isWarning;
    float currentTimer;

    protected override void Update()
    {
        base.Update();

        if (isWarning)
        {
            if (currentTimer > 0)
            {
                currentTimer -= Time.deltaTime;
                warningLights.CurrentTimer = currentTimer;
                return;
            }

            SpawnTrain();
            isWarning = false;
        }
        else
        {
            warningLights.TurnOff();
        }
    }

    protected override void Spawn()
    {
        isWarning = true;
        currentTimer = 2;
        warningLights.TurnOn();
    }

    private void SpawnTrain()
    {
        SoundManager.Instance.Train();
        var spawnPos = this.transform.position +
            Vector3.right * (isRight ? -(terrain.Extent + 1) : terrain.Extent + 1);

        var trainInstance = Instantiate
        (
            original: trainPrefab,
            position: spawnPos,
            rotation: Quaternion.Euler(0, isRight ? 90 : -90, 0),
            parent: this.transform
        );

        trainInstance.transform.localPosition -= new Vector3(0, 0, 0.3f);

        var train = trainInstance.GetComponent<Train>();
        train.Setup(terrain.Extent);
    }
}
