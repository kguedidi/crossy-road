using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    public int HighestScore;

    public bool isGameStarted = false;

    public bool startClick = false;

    [SerializeField] TMP_Text highestScoreText;

    public void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(gameObject);
        Load();
    }

    void Update()
    {
        Save();

        if(Input.GetKeyDown(KeyCode.S) && !isGameStarted)
        {
            Erase();
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt("HighestScore", HighestScore);
        PlayerPrefs.SetInt("soundOn", SoundManager.soundOn ? 1 : 0);
    }

    public void Load()
    {
        HighestScore = PlayerPrefs.GetInt("HighestScore", 0);
        highestScoreText.text = "Highest score : " + HighestScore;
        SoundManager.soundOn = PlayerPrefs.GetInt("soundOn") == 1 ? true : false;
    }

    public void Erase()
    {
        GameManager GM = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();

        HighestScore = 0;
        Save();
        startClick = true;
        GM.RestartGame();
    }
}
