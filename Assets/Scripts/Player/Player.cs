using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{
    [SerializeField] public GameManager gameManager;
    [SerializeField] private GameObject chickenAll;
    [SerializeField] private ParticleSystemRenderer splashParticle;
    [SerializeField] private TMP_Text stepText;
    [SerializeField] private TMP_Text timeText;
    [SerializeField] private TMP_Text coinCountText; 
    [SerializeField, Range(0.01f, 1f)] private float moveDuration = 0.2f;
    [SerializeField, Range(0.01f, 1f)] private float jumpHeight = 0.5f;
    [SerializeField] public int maxTravel;

    public PlayerMovement playerMovement;
    public PlayerCollision playerCollision;
    public PlayerUI playerUI;

    private void Awake()
    {
        playerMovement = new PlayerMovement(this, moveDuration, jumpHeight);
        playerCollision = new PlayerCollision(this);
        playerUI = new PlayerUI(stepText, timeText, coinCountText); 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SoundManager.Instance.Click();
            if (gameManager.isGamePaused)
            {
                gameManager.Resume();
            }
            else
            {
                gameManager.Pause();
            }
        }

        playerMovement.HandleInput();
        playerMovement.UpdatePosition();
        playerUI.UpdateUI(maxTravel);
    }

    private void OnTriggerEnter(Collider other)
    {
        playerCollision.HandleTriggerEnter(other);

        // Handle coin collection
        if (other.CompareTag("Coin"))
        {
            Destroy(other.gameObject);
            GameManager.Instance.AddCoin();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        playerCollision.HandleTriggerExit(other);
    }

    public void SetUp(int minZPos, int extent)
    {
        playerMovement.SetUp(minZPos, extent);
    }

    public void Drown()
    {
        SoundManager.Instance.Drown();
        chickenAll.SetActive(false);
        splashParticle.gameObject.SetActive(true);
        this.enabled = false;
    }

    public void Squish()
    {
        SoundManager.Instance.Squish();
        transform.DOScaleY(0.1f, 0.2f);
        transform.DOScaleX(2, 0.2f);
        transform.DOScaleZ(2, 0.2f);

        if (playerCollision.OnRail)
        {
            transform.position = new Vector3(transform.position.x, 0.036f, transform.position.z);
        }

        this.enabled = false;
    }

    public bool IsBouncing()
    {
        return DOTween.IsTweening(transform);
    }

    public int CurrentTravel => playerMovement.CurrentTravel;
    public int MaxTravel => maxTravel;
    public bool IsDie => !this.enabled;
}
