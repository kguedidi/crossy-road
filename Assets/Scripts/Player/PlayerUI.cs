using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class PlayerUI
{
    private TMP_Text stepText;
    private TMP_Text timeText;
    private TMP_Text coinCountText; 
    private float startTime;

    public PlayerUI(TMP_Text stepText, TMP_Text timeText, TMP_Text coinCountText) 
    {
        this.stepText = stepText;
        this.timeText = timeText;
        this.coinCountText = coinCountText; 
        startTime = Time.time;
    }

    public void UpdateUI(int maxTravel)
    {
        float timeElapsed = Time.time - startTime;
        int minutes = (int)(timeElapsed / 60);
        int seconds = (int)(timeElapsed % 60);
        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);

        stepText.text = "Score : " + maxTravel;
        coinCountText.text = GameManager.Instance.coinCount.ToString(); 
    }
}
