using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class PlayerMovement
{
    private Player player;
    private float moveDuration;
    private float jumpHeight;
    private char faceDir;
    private float southLimit;
    private float westLimit;
    private float eastLimit;
    private int currentTravel;

    public PlayerMovement(Player player, float moveDuration, float jumpHeight)
    {
        this.player = player;
        this.moveDuration = moveDuration;
        this.jumpHeight = jumpHeight;
    }

    public void HandleInput()
    {
        var moveDir = Vector3.zero;

        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            moveDir += new Vector3(0, 0, 1);
            faceDir = 'N';
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            moveDir += new Vector3(0, 0, -1);
            faceDir = 'S';
        }
        else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            moveDir += new Vector3(-1, 0, 0);
            faceDir = 'W';
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            moveDir += new Vector3(1, 0, 0);
            faceDir = 'E';
        }

        if (moveDir != Vector3.zero && player.IsBouncing() == false)
        {
            Bounce(moveDir, faceDir);
        }
    }

    public void UpdatePosition()
    {
        if (player.playerCollision.StandingOnTrunk && player.IsBouncing() == false)
        {
            if (player.playerCollision.TrunkIsRight)
            {
                player.transform.position += new Vector3(1, 0, 0) * Time.deltaTime * player.playerCollision.TrunkSpeed;
            }
            else
            {
                player.transform.position -= new Vector3(1, 0, 0) * Time.deltaTime * player.playerCollision.TrunkSpeed;
            }
        }

        if (!player.playerCollision.WillDrown)
        {
            if ((player.transform.position.x < -4 || player.transform.position.x > 4) && player.IsBouncing() == false)
            {
                Bounce(player.transform.position, 'X');

                if (!player.playerCollision.StandingOnTrunk)
                {
                    player.playerCollision.WillDrown = true;
                }
            }
        }

        if (!player.playerCollision.SoonDead)
        {
            if (player.playerCollision.WillDrown && player.IsBouncing() == false)
            {
                player.Drown();
                player.playerCollision.SoonDead = true;
            }
        }

        if (player.IsBouncing() == false)
        {
            if (player.playerCollision.StandingOnTrunk || player.playerCollision.StandingOnLily)
            {
                player.transform.position = new Vector3(player.transform.position.x, 0.05f, player.transform.position.z);
            }
            else
            {
                player.transform.position = new Vector3(player.transform.position.x, 0, player.transform.position.z);
            }
        }
    }

    private void Bounce(Vector3 moveDir, char faceDir)
    {
        SoundManager.Instance.Bounce();
        var movePosition = player.transform.position + moveDir;
        movePosition = new Vector3(Mathf.Round(movePosition.x), Mathf.Round(movePosition.y), Mathf.Round(movePosition.z));

        switch (faceDir)
        {
            case 'N':
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x, 0, player.transform.rotation.z);
                break;
            case 'S':
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x, 180, player.transform.rotation.z);
                break;
            case 'W':
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x, -90, player.transform.rotation.z);
                break;
            case 'E':
                player.transform.rotation = Quaternion.Euler(player.transform.rotation.x, 90, player.transform.rotation.z);
                break;
        }

        var moveSeq = DOTween.Sequence(player.transform);
        moveSeq.Append(player.transform.DOMoveY(jumpHeight, moveDuration / 2));
        moveSeq.Append(player.transform.DOMoveY(0, moveDuration / 2));

        if (movePosition.z <= southLimit ||
            movePosition.x <= westLimit ||
            movePosition.x >= eastLimit)
        {
            return;
        }

        if (Obstacle.TreePositions.Contains(movePosition) || Obstacle.RockPositions.Contains(movePosition))
        {
            if (player.gameManager.TerrainType[(int)player.transform.position.z].GetComponent<Water>() != null)
            {
                player.playerCollision.StandingOnTrunk = false;
                player.playerCollision.WillDrown = true;
                return;
            }
            else
            {
                return;
            }
        }

        player.transform.DOMoveX(Mathf.Round(movePosition.x), moveDuration);
        player.transform.DOMoveZ(movePosition.z, moveDuration)
            .OnComplete(UpdateTravel);
    }

    public void UpdateTravel()
    {
        currentTravel = (int)player.transform.position.z;

        if (currentTravel > player.MaxTravel)
        {
            player.maxTravel = currentTravel;
        }

        if (player.gameManager.TerrainType[(int)player.transform.position.z].GetComponent<Water>() != null)
        {
            if (player.gameManager.TerrainType[(int)player.transform.position.z].GetComponent<LilyPadSpawner>().enabled &&
                    !player.gameManager.TerrainType[(int)player.transform.position.z].GetComponent<LilyPadSpawner>().safeXPos.Contains((int)player.transform.position.x))
            {
                player.Drown();
            }
            else if (player.gameManager.TerrainType[(int)player.transform.position.z].GetComponent<TrunkSpawner>().enabled && player.IsBouncing() == false)
            {
                if (!player.playerCollision.StandingOnTrunk)
                {
                    player.Drown();
                }
            }
        }
    }

    public void SetUp(int minZPos, int extent)
    {
        southLimit = minZPos - 1;
        westLimit = -(extent + 1);
        eastLimit = extent + 1;
    }

    public int CurrentTravel => currentTravel;
}