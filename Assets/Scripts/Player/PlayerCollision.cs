using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using TMPro;

public class PlayerCollision
{
    private Player player;
    public bool StandingOnTrunk { get; set; }
    public bool StandingOnLily { get; set; }
    public bool OnRail { get; private set; }
    public bool TrunkIsRight { get; private set; }
    public float TrunkSpeed { get; private set; }
    public bool WillDrown { get; set; }
    public bool SoonDead { get; set; }

    public PlayerCollision(Player player)
    {
        this.player = player;
    }

    public void HandleTriggerEnter(Collider other)
    {
        if (player.enabled == false)
        {
            return;
        }

        if (other.tag == "Trunk")
        {
            TrunkIsRight = other.GetComponent<Trunk>().isRight;
            TrunkSpeed = other.GetComponent<Trunk>().GetSpeed();
            StandingOnTrunk = true;
        }

        if (other.tag == "LilyPad")
        {
            StandingOnLily = true;
        }

        if (other.tag == "Car")
        {
            SoundManager.Instance.Squish();
            player.Squish();
        }

        if (other.tag == "Rail")
        {
            OnRail = true;
        }
    }

    public void HandleTriggerExit(Collider other)
    {
        if (other.tag == "Trunk")
        {
            StandingOnTrunk = false;
            player.playerMovement.UpdateTravel();
        }

        if (other.tag == "LilyPad")
        {
            StandingOnLily = false;
        }

        if (other.tag == "Rail")
        {
            OnRail = false;
        }
    }
}