using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] Vector3 offset;

    void Start()
    {
        offset = this.transform.position - player.transform.position;
    }

    Vector3 lastPlayerPos;

    void Update()
    {
        if (lastPlayerPos == player.transform.position || player.IsDie)
        {
            return;
        }

        var targetPlayerPos = new Vector3(player.transform.position.x, 0, player.transform.position.z);

        transform.position = targetPlayerPos + offset;
        lastPlayerPos = player.transform.position;
    }
}
