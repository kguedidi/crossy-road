using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using static NewBehaviourScript;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public bool isGameStarted = false;
    public bool isGamePaused = false;

    [SerializeField] Player player;
    [SerializeField] GameObject playerObject;
    [SerializeField] int behindPlayerDistance = -5;
    [SerializeField] int frontPlayerDistance = 10;

    public bool startTerrain = false;
    public int extent = 5;
    public Dictionary<int, GameObject> TerrainType = new Dictionary<int, GameObject>();
    public Dictionary<int, TerrainBlock> map = new Dictionary<int, TerrainBlock>(50);
    [SerializeField] public int maxSameTerrainRepeat = 3;
    public bool previousWasWater = false;
    public bool trunksGoingRight;
    


    int currentHighestScore;
    public int coinCount = 0; 

    private int playerLastMaxTravel;

    [SerializeField] GameObject startUI;
    [SerializeField] GameObject pauseUI;
    [SerializeField] GameObject settingsUI;
    [SerializeField] GameObject gameUI;
    [SerializeField] GameObject gameOverUI;
    [SerializeField] TMP_Text difficultyText;

    public TMP_Text coinCountText;

    private DifficultyLevel currentDifficulty = DifficultyLevel.Medium;

    private EagleSpawner eagleSpawner;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        LoadDifficulty();
    }

    private void Start()
    {
        eagleSpawner = FindObjectOfType<EagleSpawner>();

        Time.timeScale = 0f;

        LoadCoinCount();

        if (DataManager.Instance.isGameStarted)
        {
            Resume();
            StartGame();
        }

        currentHighestScore = DataManager.Instance.HighestScore;

        if (DataManager.Instance.startClick)
        {
            DataManager.Instance.startClick = false;
        }

        gameOverUI.SetActive(false);
        UpdateCoinCountUI(); 
    }

    private void Update()
    {
        if (startTerrain)
        {
            if (player.IsDie)
            {
                gameUI.SetActive(false);
                gameOverUI.SetActive(true);
                SaveCoinCount(); 
            }
            if (player.MaxTravel == playerLastMaxTravel)
            {
                return;
            }
            playerLastMaxTravel = player.MaxTravel;

            var randTerrainBlockPrefab = TerrainGenerator.Instance.GetNextRandomTerrainPrefab(player.MaxTravel + frontPlayerDistance);
            TerrainGenerator.Instance.GenerateTerrain(randTerrainBlockPrefab, player.MaxTravel + frontPlayerDistance);

            var lastTerrainBlock = map[(player.MaxTravel - 1) + behindPlayerDistance];
            map.Remove((player.MaxTravel - 1) + behindPlayerDistance);
            Destroy(lastTerrainBlock.gameObject);
            player.SetUp(player.MaxTravel + behindPlayerDistance, extent);
        }

        if (player.MaxTravel > DataManager.Instance.HighestScore)
        {
            DataManager.Instance.HighestScore = player.MaxTravel;
        }
    }

    public void StartGame()
    {
        DataManager.Instance.isGameStarted = true;
        PrepareGame();

        playerObject.SetActive(true);
        isGameStarted = true;
        GetComponent<EagleSpawner>().enabled = true;
        eagleSpawner.enabled = true;
    }

    void PrepareGame()
    {
        startUI.SetActive(false);

        trunksGoingRight = Random.value > 0.5f ? true : false;

        for (int z = behindPlayerDistance; z <= 0; z++)
        {
            TerrainGenerator.Instance.GenerateTerrain(TerrainGenerator.Instance.grassPrefab, z);
        }

        startTerrain = true;

        for (int z = 1; z <= frontPlayerDistance; z++)
        {
            var prefab = TerrainGenerator.Instance.GetNextRandomTerrainPrefab(z);

            TerrainGenerator.Instance.GenerateTerrain(prefab, z);
        }

        player.SetUp(behindPlayerDistance, extent);
    }

    public void PlayButton()
    {
        SoundManager.Instance.Click();
        Resume();
        StartGame();
    }

    public void OptionsButton()
    {
        SoundManager.Instance.Click();
        settingsUI.SetActive(true);
        startUI.SetActive(false);
    }

    public void PreviousButton()
    {
        SoundManager.Instance.Click();
        settingsUI.SetActive(false);
        startUI.SetActive(true);
    }

    public void Resume()
    {
        pauseUI.SetActive(false);
        gameUI.SetActive(true);
        Time.timeScale = 1f;
        isGamePaused = false;
    }

    public void Pause()
    {
        pauseUI.SetActive(true);
        gameUI.SetActive(false);
        Time.timeScale = 0f;
        isGamePaused = true;
        SaveCoinCount();
    }

    public void RestartGame()
    {
        SoundManager.Instance.Click();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GoToMenu()
    {
        SoundManager.Instance.Click();
        DataManager.Instance.isGameStarted = false;
        SaveCoinCount(); 
        RestartGame();
    }

    public void Quit()
    {
        SoundManager.Instance.Click();
        SaveCoinCount(); 
        Application.Quit();
    }

    public void AddCoin()
    {
        coinCount++;
        UpdateCoinCountUI();
    }

    public void UpdateCoinCountUI()
    {
        if (coinCountText != null)
        {
            coinCountText.text = "Coins: " + coinCount;
        }
    }

    public void SaveCoinCount()
    {
        PlayerPrefs.SetInt("CoinCount", coinCount);
        PlayerPrefs.Save();
    }

    public void LoadCoinCount()
    {
        coinCount = PlayerPrefs.GetInt("CoinCount", 0);
        UpdateCoinCountUI(); 
    }

    public void ToggleDifficulty()
    {
        currentDifficulty = (DifficultyLevel)(((int)currentDifficulty + 1) % 3);
        SaveDifficulty();
        UpdateDifficultyDisplay();
        eagleSpawner.SetTimerBasedOnDifficulty();
    }

    private void UpdateDifficultyDisplay()
    {
        if (difficultyText == null)
        {
            Debug.LogError("Difficulty Text is not assigned.");
            return;
        }
        difficultyText.text = "Difficulty: " + currentDifficulty.ToString();
        Debug.Log("Current Difficulty: " + currentDifficulty);
    }

    public DifficultyLevel GetCurrentDifficulty()
    {
        return currentDifficulty;
    }

    private void SaveDifficulty()
    {
        PlayerPrefs.SetInt("DifficultyLevel", (int)currentDifficulty);
        PlayerPrefs.Save();
    }

    private void LoadDifficulty()
    {
        if (PlayerPrefs.HasKey("DifficultyLevel"))
        {
            currentDifficulty = (DifficultyLevel)PlayerPrefs.GetInt("DifficultyLevel");
        }
        else
        {
            currentDifficulty = DifficultyLevel.Medium;
        }
    }
}
