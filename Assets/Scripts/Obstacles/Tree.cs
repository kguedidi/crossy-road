using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : Obstacle
{
    protected override void OnEnable()
    {
        TreePositions.Add(transform.position);
    }

    protected override void OnDisable()
    {
        TreePositions.Remove(transform.position);
    }

    private void Update()
    {
        if (IsPositionOccupiedByRock())
        {
            Destroy(gameObject);
        }
    }

    private bool IsPositionOccupiedByRock()
    {
        return RockPositions.Contains(transform.position);
    }
}
