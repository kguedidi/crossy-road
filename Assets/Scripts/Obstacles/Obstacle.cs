using System.Collections.Generic;
using UnityEngine;

public abstract class Obstacle : MonoBehaviour
{
    public static List<Vector3> RockPositions = new List<Vector3>();
    public static List<Vector3> TreePositions = new List<Vector3>();

    protected virtual void OnEnable() {}

    protected virtual void OnDisable() {}
}
