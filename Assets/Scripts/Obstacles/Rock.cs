using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : Obstacle
{
    protected override void OnEnable()
    {
        RockPositions.Add(transform.position);
    }

    protected override void OnDisable()
    {
        RockPositions.Remove(transform.position);
    }
}
