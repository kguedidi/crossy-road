using UnityEngine;
using TMPro;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance { get; private set; }

    [SerializeField] private AudioSource playerSource;
    [SerializeField] private AudioSource carSource;
    [SerializeField] private AudioSource trainSource;
    [SerializeField] private AudioClip click, bounce, car, squish, drown, train, eagle;
    public TMP_Text soundText;
    public static bool soundOn;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        soundText.text = "Son " + (soundOn ? "activé" : "désactivé");
    }

    public void ToogleSound()
    {
        soundOn = !soundOn;
        soundText.text = "Son " + (soundOn ? "activé" : "désactivé");
    }

    public void Click()
    {
        playerSource.clip = click;
        if (soundOn) {
            playerSource.Play();
        }
    }

    public void Bounce()
    {
        playerSource.clip = bounce;
        if (soundOn) {
            playerSource.Play();
        }
    }

    public void Car()
    {
        carSource.clip = car;
        if (soundOn) {
            carSource.Play();
        }
    }

    public void Squish()
    {
        playerSource.clip = squish;
        if (soundOn) {
            playerSource.Play();
        }
    }

    public void Drown()
    {
        playerSource.clip = drown;
        if (soundOn) {
            playerSource.Play();
        }
    }


    public void Train()
    {
        trainSource.clip = train;
        if (soundOn) {
            trainSource.Play();
        }
    }

    public void Eagle()
    {
        playerSource.clip = eagle;
        if (soundOn) {
            playerSource.Play();
        }
    }
}
