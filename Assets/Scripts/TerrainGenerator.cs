using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    public static TerrainGenerator Instance { get; private set; }

    [SerializeField] public GameObject grassPrefab;
    [SerializeField] GameObject roadPrefab;
    [SerializeField] GameObject waterPrefab;
    [SerializeField] GameObject trackPrefab;
    [SerializeField] GameObject coinPrefab;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void GenerateTerrain(GameObject prefab, int zPos)
    {
        var prefabInstance = Instantiate(prefab, new Vector3(0, 0, zPos), Quaternion.identity);
        var terrainBlock = prefabInstance.GetComponent<TerrainBlock>();
        terrainBlock.Build(GameManager.Instance.extent);

        GameManager.Instance.map.Add(zPos, terrainBlock);
        GameManager.Instance.TerrainType.Add(zPos, prefabInstance);

        if (prefab == grassPrefab)
        {
            TryPlaceCoin(prefabInstance);
        }

        if (GameManager.Instance.startTerrain)
        {
            if (prefabInstance.GetComponent<Water>() != null)
            {
                if (GameManager.Instance.previousWasWater)
                {
                    if (Random.value <= 0.75f)
                    {
                        if (!GameManager.Instance.TerrainType[zPos - 1].GetComponent<LilyPadSpawner>().enabled)
                        {
                            prefabInstance.GetComponent<LilyPadSpawner>().enabled = true;
                        }
                        else
                        {
                            EnableTrunkSpawner(prefabInstance, zPos);
                        }
                    }
                    else
                    {
                        EnableTrunkSpawner(prefabInstance, zPos);
                    }
                }
                else
                {
                    prefabInstance.GetComponent<LilyPadSpawner>().enabled = true;
                }

                GameManager.Instance.previousWasWater = true;
            }

            GameManager.Instance.trunksGoingRight = !GameManager.Instance.trunksGoingRight;

            if (prefabInstance.GetComponent<Water>() == null)
            {
                GameManager.Instance.previousWasWater = false;

                if (GameManager.Instance.TerrainType[zPos - 1].GetComponent<Water>() != null)
                {
                    var waterParent = GameManager.Instance.TerrainType[zPos - 1];

                    foreach (Transform child in waterParent.transform)
                    {
                        if (child.tag == "LilyPad")
                        {
                            Destroy(child.gameObject);
                        }
                    }

                    GameManager.Instance.TerrainType[zPos - 1].GetComponent<LilyPadSpawner>().enabled = false;
                    EnableTrunkSpawner(waterParent, zPos - 1);
                }
            }
        }
    }

    private void EnableTrunkSpawner(GameObject prefabInstance, int zPos)
    {
        prefabInstance.GetComponent<TrunkSpawner>().enabled = true;
        prefabInstance.GetComponent<TrunkSpawner>().IsRight = GameManager.Instance.trunksGoingRight;
        Debug.Log("TrunkSpawner enabled on water at zPos: " + zPos);
    }

    private void TryPlaceCoin(GameObject terrainBlock)
    {
        TerrainBlock block = terrainBlock.GetComponent<TerrainBlock>();

        if (Random.value < 0.2f)
        {
            Vector3 terrainPosition = terrainBlock.transform.position;
            Vector3 coinPosition = Vector3.zero;

            for (int i = 0; i < 10; i++) 
            {
                coinPosition = terrainPosition + new Vector3(Random.Range(-block.Extent, block.Extent + 1), 0.15f, Random.Range(-block.Extent, block.Extent + 1));

                if (!block.IsPositionOccupied(coinPosition))
                {
                    Instantiate(coinPrefab, coinPosition, Quaternion.identity);
                    break;
                }
            }
        }
    }

    public GameObject GetNextRandomTerrainPrefab(int nextPos)
    {
        bool isUniform = true;
        var terrainBlockRef = GameManager.Instance.map[nextPos - 1];

        for (int distance = 2; distance <= GameManager.Instance.maxSameTerrainRepeat; distance++)
        {
            if (GameManager.Instance.map[nextPos - distance].GetType() != terrainBlockRef.GetType())
            {
                isUniform = false;
                break;
            }
        }

        if (isUniform)
        {
            if (terrainBlockRef is Grass)
            {
                if (Random.Range(1, 3) == 1)
                {
                    return roadPrefab;
                }
                else
                {
                    return waterPrefab;
                }
            }
            else if (terrainBlockRef is Road)
            {
                if (Random.Range(1, 3) == 1)
                {
                    return grassPrefab;
                }
                else
                {
                    return waterPrefab;
                }
            }
            else if (terrainBlockRef is Water)
            {
                if (Random.Range(1, 3) == 1)
                {
                    return grassPrefab;
                }
                else
                {
                    return roadPrefab;
                }
            }
            else if (terrainBlockRef is Track)
            {
                if (Random.Range(1, 3) == 1)
                {
                    return roadPrefab;
                }
                else
                {
                    return grassPrefab;
                }
            }
        }

        float random = Random.value;
        if (random <= 0.35f)
        {
            return grassPrefab;
        }
        else if (random <= 0.6f)
        {
            return roadPrefab;
        }
        else if (random <= 0.85f)
        {
            return waterPrefab;
        }
        else
        {
            return trackPrefab;
        }
    }
}
